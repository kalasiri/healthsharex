/*
 * Copyright 2023 Khalid Alasiri
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * [MIT License text]
 */

use rusqlite::{params, Connection};
use serde::Deserialize;
use serde_xml_rs::from_reader;
use std::fs::File;
use std::io::BufReader;

/// Represents a health record entry.
#[derive(Debug, Deserialize)]
struct Record {
    #[serde(rename = "date")]
    date: String,
    #[serde(rename = "type")]
    record_type: String,
    #[serde(rename = "value")]
    value: String,
}

/// Represents a collection of health records.
#[derive(Debug, Deserialize)]
struct HealthRecords {
    #[serde(rename = "record")]
    records: Vec<Record>,
}

/// Converts Apple HealthKit data from an XML file to a SQLite database.
///
/// # Arguments
///
/// * `input_file` - The path to the input XML file containing the HealthKit data.
/// * `output_file` - The path to the output SQLite database file.
///
/// # Errors
///
/// Returns an error if there are any issues during the conversion process.
pub fn convert_healthkit_data(
    input_file: &str,
    output_file: &str,
) -> Result<(), Box<dyn std::error::Error>> {
    let conn = Connection::open(output_file)?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS records (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            date TEXT,
            record_type TEXT,
            value TEXT
        )",
        [],
    )?;

    let file = File::open(input_file).expect("Failed to open file");
    let reader = BufReader::new(file);

    // Deserialize XML to Rust structs
    let health_records: HealthRecords = from_reader(reader).expect("Failed to parse XML");

    // Insert records into the database
    for record in health_records.records {
        conn.execute(
            "INSERT INTO records (date, record_type, value)
             VALUES (?, ?, ?)",
            params![record.date, record.record_type, record.value],
        )
        .expect("Failed to insert record");
    }

    Ok(())
}
