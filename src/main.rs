/*
 * Copyright 2023 Khalid Alasiri
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * [MIT License text]
 */

#[macro_use]
extern crate rocket;
extern crate rocket_dyn_templates;

use convert_healthkit_data::*;
use rocket_dyn_templates::context;
use rocket_dyn_templates::Template;
use rusqlite::{params, Connection};
use serde::Serialize;

#[get("/")]
fn index() -> Template {
    Template::render(
        "index",
        context! {
            page_title: "Index",
        },
    )
}

#[get("/home")]
fn home() -> Template {
    Template::render(
        "home",
        context! {
            page_title: "Home",
        },
    )
}

#[get("/login")]
fn login() -> Template {
    Template::render(
        "login",
        context! {
            page_title: "Login",
        },
    )
}

#[get("/registration")]
fn registration() -> Template {
    Template::render(
        "registration",
        context! {
            page_title: "Registration",
        },
    )
}

#[get("/upload")]
fn upload() -> Template {
    Template::render(
        "upload",
        context! {
            page_title: "Upload",
        },
    )
}

/// Uploads a HealthKit data file to the server.
///
/// # Parameters
///
/// * `input_file`: The path to the HealthKit data file.
/// * `output_file`: The path to the output database file.
///
/// # Returns
///
/// A `Result` of either `()` on success or an `anyhow::Error` on failure.
#[post("/upload/record-saving")]
async fn upload_record_saving() -> Template {
    let input_file = "Records/Sample.xml";
    let output_file = "Records/output.db";

    let result = convert_healthkit_data(input_file, output_file);

    let message = if result.is_err() {
        result.err().unwrap().to_string()
    } else {
        "HealthKit data converted successfully!".to_string()
    };

    Template::render(
        "message",
        context! {
            message: message,
        },
    )
}

#[derive(Serialize)]
struct Record {
    date: String,
    record_type: String,
    value: String,
}

#[derive(Serialize)]
struct IndexContext {
    records: Vec<Record>,
}

#[get("/results")]
fn results() -> Template {
    let conn = Connection::open("Records/output.db").expect("Failed to open database");

    let mut stmt = conn
        .prepare("SELECT date, record_type, value FROM records")
        .expect("Failed to prepare statement");
    let rows = stmt
        .query_map(params![], |row| {
            Ok(Record {
                date: row.get(0)?,
                record_type: row.get(1)?,
                value: row.get(2)?,
            })
        })
        .expect("Failed to query records");

    let records: Vec<Record> = rows.map(|row| row.unwrap()).collect();

    let context = IndexContext { records };

    Template::render("results", &context)
}

#[get("/share")]
fn share() -> Template {
    Template::render(
        "share",
        context! {
            page_title: "Share",
        },
    )
}

#[post("/share/results")]
fn share_results() -> Template {
    let conn = Connection::open("Records/output.db").expect("Failed to open database");

    let query = "SELECT date, record_type, value FROM records WHERE record_type = 'Blood Pressure'";

    let mut stmt = conn.prepare(query).expect("Failed to prepare statement");
    let rows = stmt
        .query_map(params![], |row| {
            Ok(Record {
                date: row.get(0)?,
                record_type: row.get(1)?,
                value: row.get(2)?,
            })
        })
        .expect("Failed to query records");

    let records = rows.filter_map(|row| row.ok()).collect();

    let context = IndexContext { records };

    Template::render("results", &context)
}

#[launch]
fn rocket() -> _ {
    let rocket = rocket::build();

    rocket.attach(Template::fairing()).mount(
        "/",
        routes![
            index,
            home,
            login,
            registration,
            upload,
            upload_record_saving,
            share,
            share_results,
            results
        ],
    )
}
