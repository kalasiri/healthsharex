# HealthShareX - Secure Health Data Sharing Tool

**Authors:** [Khalid Alasiri](mailto:kalasiri@pdx.edu)

## Description

HealthShareX is a secure health data sharing tool built using the Rust programming language. This tool enables users to share their health data from Apple Health with their health providers with specific access restrictions such as limited time and anonymization. The focus of this project is to address the security and privacy of the health record, ensuring only the specialist can access particular data for a limited time.

HealthShareX aims to address these concerns by providing a secure health data-sharing tool that puts users in control of their data. The vision for HealthShareX is to become the go-to platform for secure health data sharing, trusted by users and healthcare providers alike. We aim to build a tool that is easy to use, efficient, and effective in protecting users' privacy.

## Build and Run

To use the tool, follow the instructions below:

1. Clone the project repository:

   ```shell
   git clone https://gitlab.cecs.pdx.edu/kalasiri/healthsharex
   ```

2. Update the `Rocket.toml` file with the IP address and port for the server to run.

3. Install the Rust programming language and Cargo, the Rust package manager, on your system.

4. Build and run the project using Cargo:

   ```shell
   cd healthsharex
   cargo run
   ```

   This will compile the project and start the server.

## Testing

Testing was done to ensure the project works as expected. A sample XML file is provided in the `/Records` directory to insert some data and use it to verify the functionality.

## Project Status

The following features are currently working:

- Parsing function to parse XML data and store it in an SQLite database using `convert_healthkit_data()`.
- Basic routes for uploading and displaying health records.

Challenges encountered during development include handling the large size of Apple Health data during the uploading process. Future work includes completing the uploading function and implementing the QR code sharing view.

We are satisfied with the current progress of the project and are committed to improving it further to meet our goals.

## Video Presentation
[PRESENTATION](PRESENTATION.mp4).



## Resources: 

- [File upload and download in Rust](https://blog.logrocket.com/file-upload-and-download-in-rust/)
- [Learn how to read a file in Rust](https://blog.logrocket.com/how-to-read-files-rust/)
- [Parse huge XML files quick with Rust + Serde + quick-xml](https://capnfabs.net/posts/parsing-huge-xml-quickxml-rust-serde/)
- [xml-and-rust](https://apiraino.github.io/xml-parsing/)
- [XML parsing in Rust](https://mainmatter.com/blog/2020/12/31/xml-and-rust/)



## License

This project is licensed under the [MIT License](LICENSE.txt).